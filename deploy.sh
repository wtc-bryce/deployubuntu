#!/bin/bash
### RTPS AUTOMATED LINUX CONFIGURATION DEPLOYMENT SYSTEM
### This script last modified on 03-AUG-2016

### SYSTEM CONFIGURATION BASICS ###
ROOTPUBKEY="ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAqYwK79E6TUmcvg0quGoARx2ulZZ8txucnHrx9fae7/gCt0ShAJx6ZDY3ghsFjlBo8DqLPKxNN47aiCRcNniViHUIBs1cV02lZm8xg3UvekEdCtdpInJwJr3pTegYKCPO733yv70u/0Kzh/Smty7zwXIvG94MMW7/p87NaZ7pzAzzaVuj+kjvaQt85qW1Uij6JsgtZ7I085X6NHRa2FvB0IXVc+qOY6kr8ZleU528FIzwn3IXezxsDBdykgKS12P294z4nK1mBhrt1lB77FVLcwt9J2dPYy+uFzGNTJG8opV8x2WqQ28ED18dgNxZuZ3lLXZsnUZXiBRCSfIwa3L96Q== Emergency Root Certificate"

USER="lawsonb"
USERPUBKEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7LFsT3jC+sa+nxYb4txViUXQBfPwIGQ/YTdG4iUdyqZ2K1nm0+KWhVGKp/owE41EGkvJ7016o2DF5GDn250JxIPT81kiEB3VIJOQYBgFBNIVapUohvauM4jAdHzTsFnbk+GdYyK64tNWMe0FR3Pjb3AhA+e2K7XlUT4hgtOx/RTZKm2tDdurwdSRmKoamt/oIrMjOVVfBZFsqiIjgDMoVDza7O9oY48DqQPJtjImXGF7l4IXqXALMtLFnVYRZLcjMUvnoFb3Zer3cPbVfu62d3SecGKueRm+iyTzUlUAiYBRMi7oA/A2Uf1tQXKpfGrl1rliEZBCGNUrcFkgOTwwV lawsonb@risingtide.services"

DOMAIN="ad.rtps.link"
HOSTNAME="ubuntu"

SNMP_ALLOW_RANGE="10.1.1.0/24"

# DO NOT MODIFY UNLESS NECESSARY >>>
SSHD_CONFIG_BASE="https://bitbucket.org/rtps-lawsonb/deployubuntu/raw/master/sshd_config.txt"
ISSUE_BASE="https://bitbucket.org/rtps-lawsonb/deployubuntu/raw/master/issue.txt"
MOTD_BASE="https://bitbucket.org/rtps-lawsonb/deployubuntu/raw/master/motd.txt"
BASHRC_BASE="https://bitbucket.org/rtps-lawsonb/deployubuntu/raw/master/bashrc.txt"
SNMPD_CONFIG_BASE="https://bitbucket.org/rtps-lawsonb/deployubuntu/raw/master/snmpd.conf.txt"

sshd_config="/tmp/rtps_sshd_config"
issue="/tmp/rtps_issue"
motd="/tmp/rtps_motd"
bashrc="/tmp/rtps_bashrc"
snmpd_config="/tmp/rtps_snmpd_config"
#                                <<<

# Pushover Alerting Configuration
function push {
    curl -s -F "token=aW1HYtKGtCmNQp862eh4DZDFB7DDVM" \
    -F "user=uMyzSHcPc6t3AoSq6LgfmfnT6K8o52" \
    -F "title=Deployed $HOSTNAME.$DOMAIN" \
    -F "message=$1" https://api.pushover.net/1/messages.json \
	>> /dev/null
}
# End Pushover configuration

########### BEGIN CORE SCRIPT #########
# Ensure we're running as root
if [ "$(whoami)" != 'root' ]; then
	echo "ERROR: You must run \"bash $0 [DesiredHostname]\" as root or using sudo!"
	exit 1;
fi

# Load HOSTNAME from argument 1.
if [ -z "$1" ]; then
	echo -n "Enter desired domain (_hostname_._domain_): "
	read DOMAIN
	echo -n "Enter desired hostname (_hostname_.$DOMAIN): "
	read HOSTNAME
else
	HOSTNAME=$1
fi

# Display upcoming configuration
echo "	%%% CONFIRM CONFIGURATION %%%
	System hostname: $HOSTNAME
	System FQDN: $HOSTNAME.$DOMAIN
	Auto-Allow IP: $ALLOWIP (Usually your SSH client's IP)
	Administrative User: $USER
	Root Pubkey: Yes
	Administrative User Pubkey: Yes
	SSHD Config: Yes
	Issue/Issue.net Config: Yes
	MOTD Config: Yes
	Bashrc Config: Yes
	SNMPD Install: Yes
	SNMPD Config: Yes
	
Press any key to confirm. Press CTRL+C to cancel."
read keypress
echo "Configuration confirmed!"

# Download configuration files
echo "Downloading configuration files"
wget --show-progress $SSHD_CONFIG_BASE -O $sshd_config --no-check-certificate
wget --show-progress $ISSUE_BASE -O $issue --no-check-certificate
wget --show-progress $MOTD_BASE -O $motd --no-check-certificate
wget --show-progress $BASHRC_BASE -O $bashrc --no-check-certificate
wget --show-progress $SNMPD_CONFIG_BASE -O $snmpd_config --no-check-certificate

# Configure root ssh key
echo "Configuring root SSH key"
mkdir /root/.ssh
echo $ROOTPUBKEY >> /root/.ssh/authorized_keys

# Reconfigure sshd
echo "Configuring sshd"
cp -f $sshd_config /etc/ssh/sshd_config
service ssh restart

# Reconfigure issue, issue.net, and motd
echo "Overwriting existing issue, issue.net, and motd"
cp -f $issue /etc/issue
cp -f $issue /etc/issue.net
cp -f $motd /etc/motd

### ADMINISTRATIVE USER ACCOUNT ###
# Create the administrative user
echo "Creating administrative user"
useradd -G sudo -s /bin/bash -Um $USER
mkdir /home/$USER/.ssh
echo $USERPUBKEY >> /home/$USER/.ssh/authorized_keys
cp $bashrc /root/.bashrc
cp $bashrc /home/$USER/.bashrc

### SET UP SYSTEM HOSTNAME ###
hostname $HOSTNAME.$DOMAIN
echo "$HOSTNAME.$DOMAIN" > /etc/hostname
echo "127.0.1.1		$HOSTNAME" >> /etc/hosts
echo "127.0.1.1		$HOSTNAME.$DOMAIN" >> /etc/hosts

### CHECK FOR DEPLOYMENT USER FROM DIRTY IMAGE ###
getent passwd user > /dev/null 2&>1
if [ $? -eq 0 ]; then
	echo "Deleting 'user' password"
	passwd -l user
	echo "Expiring 'user' account"
	usermod --expiredate 1 user
	echo "Removing 'user' .ssh directory and keys"
	rm -rf /home/user/.ssh
fi


### UBUNTU-SPECIFIC MODIFICATIONS ###
chmod -x /etc/update-motd.d/10-help-text

### SET UP FIREWALL (UFW) ###
ufw allow ssh
ufw allow 22
sudo ufw allow from $SNMP_ALLOW_RANGE to any port 161
ufw --force enable

### EXPIRE ROOT PASSWORD AND SET UP SUDO ###

echo "The root password has been disabled. You must now use the administrative user."
passwd -l root
bash -c 'echo "%sudo ALL=(ALL:ALL) NOPASSWD:ALL" | (EDITOR="tee -a" visudo)'

### UPDATE REPOS, INSTALL/CONFIGURE SOME BASIC SOFTWARE ###
sudo apt-get update
sudo apt-get -yq install ntp htop snmpd unzip
cp -f $snmpd_config /etc/snmp/snmpd.conf

push "The deployment has completed. Check server for status."

echo "\n\nThe script has been completed."

### MISCELLANEOUS CLEANUP & TASKS ###


### END OF CORE SCRIPT